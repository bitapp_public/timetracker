#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <cstring>
#include <string>
#include <time.h>
#include <locale>
#include <codecvt>
#include <direct.h>
using namespace std;

BOOL IsProcessRunning(DWORD pid)
{
	HANDLE process = OpenProcess(SYNCHRONIZE, FALSE, pid);
	DWORD ret = WaitForSingleObject(process, 0);
	CloseHandle(process);
	return ret == WAIT_TIMEOUT;
}


DWORD FindProcessId(const std::wstring& processName)
{
	PROCESSENTRY32 processInfo;
	processInfo.dwSize = sizeof(processInfo);

	HANDLE processesSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (processesSnapshot == INVALID_HANDLE_VALUE)
		return 0;

	Process32First(processesSnapshot, &processInfo);
	if (!processName.compare(processInfo.szExeFile))
	{
		CloseHandle(processesSnapshot);
		return processInfo.th32ProcessID;
	}

	while (Process32Next(processesSnapshot, &processInfo))
	{
		if (!processName.compare(processInfo.szExeFile))
		{
			CloseHandle(processesSnapshot);
			return processInfo.th32ProcessID;
		}
	}

	CloseHandle(processesSnapshot);
	return 0;
}

int getProcessID(wstring processName)
{

	DWORD processID = FindProcessId(processName);

	if (!processID == 0)
		std::wcout << "Found process with ID " << processID << std::endl;
	return processID;
	system("PAUSE");

}

void formatTime(int* arrpt, int totalseconds) {

	int hours, minutes, seconds;

	minutes = totalseconds / 60;
	seconds = int(totalseconds) % 60;
	hours = minutes / 60;
	minutes = minutes % 60;

	*arrpt = hours;
	arrpt++;
	*arrpt = minutes;
	arrpt++;
	*arrpt = seconds;

}

int main()
{
	time_t timer;
	time_t current;
	int runtime = 0;
	int runtimetotal = 0;
	int procID = 0;
	bool setNewClock = true;
	bool searchProcess = true;
	bool applicationClosed = false;
	char processName[50];
	int currentSessionTime[3];
	int totalTime[3];

	char path[256];
	char temp[256];
	_getcwd(temp, 256);
	strcpy_s(path, temp);
	strcat_s(path, "\\config.ini");

	int sleepTime = GetPrivateProfileIntA("config", "sleepTime", 10000, path);
	GetPrivateProfileStringA("config", "exeName", "starcitizen.exe", processName, 50, path);

	totalTime[0] = GetPrivateProfileIntA("tracking", "hours", 0, path);
	totalTime[1] = GetPrivateProfileIntA("tracking", "minutes", 0, path);
	totalTime[2] = GetPrivateProfileIntA("tracking", "seconds", 0, path);
	runtimetotal = totalTime[0] * 3600 + totalTime[1] * 60 + totalTime[2];

	size_t origsize = strlen(processName) + 1;
	const size_t newsize = 100;
	size_t convertedChars = 0;
	wchar_t wcstring[newsize];
	mbstowcs_s(&convertedChars, wcstring, origsize, processName, _TRUNCATE);
	wstring input(wcstring);

	cout << "Time between updates: " << sleepTime << endl;
	wcout << "Searching for " << input << "..." << endl;

	while (true) {
		if (searchProcess) {
			procID = getProcessID(input);
			searchProcess = false;
		}

		if (procID != 0 && IsProcessRunning(procID)) {
			if (setNewClock) {
				time(&timer);
				setNewClock = false;
				applicationClosed = true;
			}
			time(&current);
			runtime = current - timer;
			formatTime(currentSessionTime, runtime);
			formatTime(totalTime, runtimetotal + runtime);
			std::cout << "\rCurrent Session: " << currentSessionTime[0] << ":" << currentSessionTime[1] << ":" << currentSessionTime[2] << " Total Time: " << totalTime[0] << ":" << totalTime[1] << ":" << totalTime[2];
			//std::cout << "\rSeconds: " << runtime << " Current: " << current << " Beginning: " << timer;

		}
		else {
			if (applicationClosed) {
				std::cout << "\n";
				runtimetotal += runtime;
				applicationClosed = false;
				formatTime(totalTime, runtimetotal);

				WritePrivateProfileStringA("tracking", "hours", (to_string(totalTime[0])).c_str(), path);
				WritePrivateProfileStringA("tracking", "minutes", (to_string(totalTime[1])).c_str(), path);
				WritePrivateProfileStringA("tracking", "seconds", (to_string(totalTime[2])).c_str(), path);
				wcout << "Searching for " << input << "..." << endl;
			}
			searchProcess = true;
			setNewClock = true;
		}
		Sleep(sleepTime);
	}
	return 0;
}